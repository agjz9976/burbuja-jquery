var $div = $('.palo-left');
var $div2 = $('.palo-right');
$(document).keydown(function() {
    switch (event.which) {
    case 87:
        $div.css('top', $div.offset().top - 10);
        break;
    case 83:
        $div.css('top', $div.offset().top + 10);
        break;
    case 40:
        $div2.css('top', $div2.offset().top + 10);
       break;
    case 38:
        $div2.css('top', $div2.offset().top - 10);
        break;
    }
});


var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var ballRadius = 10;
var x = canvas.width/2;
var y = canvas.height-30;
var dx = 2;
var dy = -2;

function drawBall() {
    ctx.beginPath();
    ctx.arc(x, y, ballRadius, 0, Math.PI*2);
    ctx.fillStyle = "#0095DD";
    ctx.fill();
    ctx.closePath();
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawBall();

    if(x + dx > canvas.width-ballRadius || x + dx < ballRadius) {
        dx = -dx;
    }
    if(y + dy > canvas.height-ballRadius || y + dy < ballRadius) {
        dy = -dy;
    }

    x += dx;
    y += dy;
}

setInterval(draw, 10);
